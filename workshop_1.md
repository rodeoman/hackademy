## Hackademy workshop 1 assignment

### Create a simple registration form

Form fields should be:

* Email
* Phone
* Password

Form should have client-side validation just using vanilla javascript. Before submitting the form we should check if all fields are filled as required.

* Email should be a valid email address and not empty
* Phone should bea a valid Estonian mobile number. It should be 7 or 8 digits long and starting with 5. Empty phone field is not allowed.
* Password should contain at least one capital letter and one digit and minimum length is 8 chars

When user hits submit button the form is being validated.
If one or more fields are invalid show an alert message. Alert message should contain validation error and field label
